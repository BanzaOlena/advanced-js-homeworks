/** @format */

"use strict";

const btn = document.querySelector("#btn");

async function getIP() {
  const url = "https://api.ipify.org/?format=json";
  const response = await fetch(url);
  const data = await response.json();

  return data;
}

async function getAdress(data) {
  const { ip: query } = data;

  const url = `http://ip-api.com/json/${query}?fields=continent,country,regionName,city,district`;
  const response = await fetch(url);
  const answer = await response.json();
  return answer;
}

function renderAnswer(answer) {
  const { continent, country, regionName, city, district } = answer;
  const text = document.createElement("p");
  text.classList.add("text");
  text.textContent = ` Your continent: ${continent}, your country: ${country}, your region: ${regionName}, your city: ${city}, your district: ${district}. I have found you!`;
  btn.after(text);
}

async function findIP() {
  const ip = await getIP();
  const information = await getAdress(ip);
  renderAnswer(information);
  btn.remove();
}

btn.addEventListener("click", findIP);
