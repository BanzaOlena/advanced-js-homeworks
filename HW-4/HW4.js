class Films {
    constructor(url) {
        this.url = url;
    }

    getFilms() {
        return fetch(url)
            .then(resolve => resolve.json());
    }

    printFilmsToPage(films) {
        films.forEach(film => {
            let ul = document.createElement('ul');
            ul.setAttribute('id', film.episodeId);

            let liId = document.createElement('li');
            liId.setAttribute('id', 'episodeId');
            liId.innerText = film.episodeId;
            ul.appendChild(liId)

            let liName = document.createElement('li');
            liName.setAttribute('id', 'name');
            liName.innerText = film.name;
            ul.appendChild(liName)

            let liOpeningCrawl = document.createElement('li');
            liOpeningCrawl.setAttribute('id', 'openingCrawl');
            liOpeningCrawl.innerText = film.openingCrawl;
            ul.appendChild(liOpeningCrawl)

            document.body.appendChild(ul)
        });
    }
}

class Film {

    getCharacters(film) {
        return Promise.all(film.characters.map(url => { return fetch(url) }))
            .then(resolveArray => Promise.all(resolveArray.map(resolve => resolve.json())))
    }

    printCharactersToPage(film, characters) {
        let characterUl = document.createElement('ul')
        characters.forEach(character => {
            let li = document.createElement('li')
            li.innerText = character.name
            characterUl.appendChild(li)
        });

        let filmUl = document.getElementById(film.episodeId);
        let filmLiArray = filmUl.getElementsByTagName('li')

        for (const li of filmLiArray) {
            if (li.id === 'name') {
                li.appendChild(characterUl)
            }
        }
    }
}


const url = 'https://ajax.test-danit.com/api/swapi/films'
const films = new Films(url);
const data = films.getFilms();
data.then(downloadedFilms => {
    films.printFilmsToPage(downloadedFilms)
    const film = new Film();
    for (const f of downloadedFilms) {
        let charArray = film.getCharacters(f);
        charArray.then(characters => {
            film.printCharactersToPage(f, characters)
        });

    }
}
);