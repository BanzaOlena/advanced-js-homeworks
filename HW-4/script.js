/** @format */

"use strict";

const URL = "https://ajax.test-danit.com/api/swapi/films";

function getFilm(url) {
  return fetch(url).then((response) => response.json());
}

function renderFilms(films) {
  let filmList = document.createElement("ul");
  films.forEach(({ episodeId, name, openingCrawl }) => {
    let itemFilm = document.createElement("li");
    itemFilm.id = name;
    itemFilm.textContent = `Episod: ${episodeId} : ${name} . ${openingCrawl} `;
    filmList.append(itemFilm);
  });
  document.body.append(filmList);
}

function getCharacters(film) {
  let promisArray = film.characters.map((link) =>
    fetch(link).then((response) => response.json())
  );
  return Promise.all(promisArray);
}

function renderCharacters(film, characters) {
  let filmLi = document.getElementById(film.name);
  let character = document.createElement("p");
  let content = "Characters:";
  characters.forEach((character) => {
    content += character.name + " ";
  });
  character.textContent = content;
  filmLi.append(character);
}

getFilm(URL).then((films) => {
  renderFilms(films);
  films.forEach((film) => {
    getCharacters(film).then((characters) =>
      renderCharacters(film, characters)
    );
  });
});
