/** @format */

"use strict";

class Cards {
  constructor(users, posts) {
    this.users = users;
    this.posts = posts;
  }
  renderPosts() {
    const root = document.getElementById("root");
    this.posts.forEach(({ id, title, userId, body }) => {
      const postDiv = document.createElement("div");
      postDiv.classList.add("card");
      postDiv.id = id;
      const postTitle = document.createElement("h3");
      postTitle.textContent = title;
      postTitle.classList.add("title");
      const postBody = document.createElement("p");
      postBody.classList.add("text");
      postBody.textContent = body;

      let user = this.users.find((user) => user.id === userId);

      let userInfo = document.createElement("p");
      userInfo.classList.add("info");
      userInfo.textContent = `${user.name}   ${user.email}`;

      let button = document.createElement("button");
      button.textContent = "DELETE";
      button.classList.add("button");
      button.addEventListener("click", () => {
        let urlDelete = `https://ajax.test-danit.com/api/json/posts/${id}`;
        try {
          fetch(urlDelete, { method: "DELETE" }).then((response) => {
            if (!response.ok) {
              console.log("Network request error");
            } else {
              let card = document.getElementById(id);
              card.remove();
              console.log(`Post ${id} was deleted`);
            }
          });
        } catch (e) {
          console.log(e.message);
        }
      });

      postDiv.append(postTitle, postBody, userInfo, button);
      root.append(postDiv);
    });
  }
}

const urlUsers = "https://ajax.test-danit.com/api/json/users";
const urlPosts = "https://ajax.test-danit.com/api/json/posts";

function getData(url) {
  return fetch(url).then((response) => response.json());
}

const usersPromise = getData(urlUsers);
const postsPromise = getData(urlPosts);

Promise.all([usersPromise, postsPromise]).then((dataArray) => {
  let cards = new Cards(dataArray[0], dataArray[1]);
  cards.renderPosts();
});
