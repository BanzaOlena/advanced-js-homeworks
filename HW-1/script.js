/** @format */

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(newName) {
    this._name = newName;
  }
  get age() {
    return this._age;
  }
  set age(newAge) {
    this._age = newAge;
  }
  get salary() {
    return this._salary;
  }
  set salary(newSalary) {
    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer("James", "25", "1000", "js");

const programmer2 = new Programmer("Fil", "30", "2000", "sql");

console.log(programmer1);
console.log(programmer2);
console.log(programmer1.salary);
console.log(programmer2.salary);
