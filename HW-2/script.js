/** @format */

"use strict";
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.getElementById("root");
const list = document.createElement("ul");

const bookWithMaxFields = getBookWithMaxFiels(books);

books.forEach((book) => {
  try {
    allFieldsExists(bookWithMaxFields, book);
    const newEl = document.createElement("li");
    newEl.textContent = createEl(book);
    list.append(newEl);
  } catch (ex) {
    console.log("Field " + ex.message + " does not exist ");
  }
});

root.append(list);

function getBookWithMaxFiels(books) {
  let rez = {};
  books.forEach((book) => {
    if (Object.keys(book).length > Object.keys(rez).length) {
      rez = book;
      console.log(rez);
    }
  });
  return rez;
}

function createEl(book) {
  let li = "";
  Object.keys(book).forEach((key) => {
    li += key + " : " + book[key] + " ";
  });
  return li;
}

function allFieldsExists(baseObject, checkedObject) {
  Object.keys(baseObject).forEach((field) => {
    if (!Object.keys(checkedObject).includes(field)) {
      throw new Error(field);
    }
  });
}
